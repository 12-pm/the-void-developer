﻿using System;
using FileSO.Player;
using Interface;
using Manager_Scripts.OtherManager;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager_Scripts.Player
{
    public class PlayerManager : MonoBehaviour,IDamage,ISPlayerDoWork,IPlayerLv,IHungry
    {
        public bool isSelect;
        public bool isDoWork;
        public bool isGenerate;
        private SpriteRenderer m_Rent;
        internal float vision;
        internal float speed;
        [SerializeField]internal float KeepTime;
        [SerializeField] internal Animator animWalk;
        [SerializeField] internal Transform shootPoint;
        internal float FireRate;
        [SerializeField] private int maxHp;
        [SerializeField] private int hp;
        [SerializeField] private float maxHungry;
        [SerializeField] private float hungry;
        [SerializeField] private float hungryReduce;
        private new string name;
        internal int damage;
        [SerializeField] private PlayerSo[] playerSo;
        [SerializeField]internal int playerLv = 0;
        private int cuurentLv;
        [SerializeField] private Direction direction;
        private float _hungryTime;
        private float _reduceHpRate;
        private float _plusHpRate;

        private AudioSource _audioSource;
        public AudioClip _audioClipDead;
        private AudioClip _audioClip;
        //Slider
        [SerializeField] private Slider hpSlider;
        [SerializeField] private Slider hungrySlider;
        [SerializeField] private GameObject sliderCanvas;
        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();
            playerLv = 0;
            _hungryTime = 0;
            SetPlayerSo();
            hp = maxHp;
            hungry = maxHungry;
            gameObject.name = name+GameManager.Instance.ListThePlayer();
            GameManager.Instance.SetNameAndAdd(gameObject.name);
            cuurentLv = playerLv;
            m_Rent = GetComponent<SpriteRenderer>();
            SetSlider();
        }
        void Update()
        {
            SetSlider();
            _hungryTime += Time.deltaTime;
            _reduceHpRate += Time.deltaTime;
            _plusHpRate += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log(playerLv);
                if (playerLv<playerSo.Length-1)
                {
                    playerLv += 1;
                }
            }
            if (isSelect)
            {
                m_Rent.color = Color.yellow;
                UIManager.Instance.Name = name;
            }
            else if(isGenerate)
            {
                gameObject.layer = LayerMask.NameToLayer("OnWork");
                m_Rent.color = Color.cyan;
            }
            else if (isDoWork)
            {
                m_Rent.color = Color.blue;
            }
            else
            {
                gameObject.layer = LayerMask.NameToLayer("Player");
                if (playerLv>=0)
                {
                    m_Rent.color = Color.white;
                }
            }
            if (playerLv!=cuurentLv)
            {
                SetPlayerSo();
                cuurentLv = playerLv;
            }

            if (GameManager.Instance.gameEnd && UIManager.Instance.isClickQuit)
            {
                direction.DestroyDirecUI();
                GameManager.Instance.DestroyPlayer(gameObject.name);
                Destroy(gameObject);
            }

            if (_hungryTime > maxHungry & hungry>=0)
            {
                HungryReduce(hungryReduce);
                _hungryTime = 0;
            }

            if (hungry<=0 & _reduceHpRate>=4.0f)
            {
                Damage(2);
                _reduceHpRate = 0;
            }

            if (hungry>=maxHungry/1.25 & _plusHpRate>=10.0f)
            {
                HpPlus(3);
                _plusHpRate = 0;
            }
        }

        private void SetSlider()
        {
            sliderCanvas.transform.eulerAngles = new Vector3(0,0,0);
            hpSlider.maxValue = maxHp;
            hpSlider.value = hp;
            hungrySlider.maxValue = maxHungry;
            hungrySlider.value = hungry;
        }

        private void HungryReduce(float other)
        {
            if (hungry!=0)
            {
                hungry -= other;
            }
            else
            {
                hungry = 0;
            }
        }

        public void HungryPlus(float other)
        {
            if (hungry<=maxHungry)
            {
                hungry += other;
            }
            if (hungry>=maxHungry)
            {
                hungry = maxHungry;
            }
        }

        public void HpPlus(int other)
        {
            if (hp<=maxHp)
            {
                hp += other;
            }
            if (hp>=maxHp)
            {
                hp = maxHp;
            }
        }
        public void Damage(int otherAttack)
        {
            hp -= otherAttack;
            if (hp<=0)
            {
                animWalk.SetBool($"Dead{playerLv}",true);
                animWalk.SetBool($"Walk{playerLv}",false);
                animWalk.SetBool($"Idle{playerLv}",false);
                animWalk.SetBool($"Attack{playerLv}",false);
                direction.DestroyDirecUI();
                Destroy(gameObject,1f);
                
                
                _audioSource.PlayOneShot(_audioClipDead);
                
                
                GameManager.Instance.DestroyPlayer(gameObject.name);
                if (GameManager.Instance.playerList.Count<=0)
                {
                    GameManager.Instance.GameLose();
                }
            }
        }

        
        void SetPlayerSo()
        {
            FireRate = playerSo[playerLv].fireRate;
            maxHp = playerSo[playerLv].maxHp;
            name = playerSo[playerLv].name;
            damage = playerSo[playerLv].damage;
            vision = playerSo[playerLv].vision;
            speed = playerSo[playerLv].speed;
            maxHungry = playerSo[playerLv].maxHungry;
            hungryReduce = playerSo[playerLv].hungryReduce;
            _audioClip = playerSo[playerLv].Sound;
            
            animWalk.SetInteger("Lv",playerLv);
        }
        
        public bool IsDoingWork()
        {
            
            return isDoWork;
        }
        public bool IsDoingWorkSet(bool setBool)
        {
            isDoWork = setBool;
            return isDoWork;
        }

        public bool IsGenerate()
        {
            return isGenerate;
        }
        

        public bool IsGenerateSet(bool setBool)
        {
            isGenerate = setBool;
            return isGenerate;
        }

        public void SetPlayerLv(int lv)
        {
            playerLv = lv;
        }

        public int PlayerLv()
        {
            return playerLv;
        }

        public void Shot()
        {
            _audioSource.PlayOneShot(_audioClip);
        }
    }
    
}

