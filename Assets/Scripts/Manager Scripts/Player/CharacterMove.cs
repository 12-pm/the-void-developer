﻿using Interface;
using Manager_Scripts.OtherManager;
using UnityEngine;
using TouchPhase = UnityEngine.TouchPhase;

namespace Manager_Scripts.Player
{
    public class CharacterMove : MonoBehaviour
    {
        [SerializeField] private float speed = 3f;
        [SerializeField] private float climb = 14f;
        [SerializeField] private Direction direcUI;
        private Vector3 _target;
        private Vector3 _touch;
        private Vector3 _ladder;
        private Touch _touchScreen;
        private bool _checkMove;
        private bool _checkLadder;
        private bool _checkTwoFloor;
        private bool _checkPlayerOut;
        private bool _checkClimbInOut;
        private Vector2 _setY; 
        private int _bitMask;
        [SerializeField]private PlayerManager player;
        private RaycastHit2D _hit;
        private RaycastHit2D _hitLadder;
        

        private void Start()
        {
            var position = transform.position;
            _target = position;
            _ladder = position;
            player.animWalk = GetComponent<Animator>();
            player.animWalk.SetBool($"Walk{player.playerLv}",false);
            player.animWalk.SetBool($"Attack{player.playerLv}",false);
            player.animWalk.SetBool($"Idle{player.playerLv}",true);
        }

        void Update()
        {
            speed = player.speed;
            if (Input.touchCount>0)
            {
                _touchScreen = Input.GetTouch(0);
                if (Camera.main is { }) _touch = Camera.main.ScreenToWorldPoint(_touchScreen.position);
                _touch.z = 0f;
            }

            if (Camera.main is { })
                _hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (_hit)
            {
                Selection();
            }
            CheckToLadder();
            if (player.isSelect)
            {
                if (Input.touchCount>0)
                {
                    SetPosition();
                }
            }
            if (_checkMove)
            {
                Move();
            }
            if (_checkLadder)
            {
                MoveToLadder();
            }
            FindLadderRight();
            FindLadderLeft();
        }
        private void FixedUpdate()
        {
            if (_checkClimbInOut)
            {
                Climbing();
            }
        }
        void Selection()
        {
            if (_hit.transform.name == gameObject.name && _touchScreen.phase == TouchPhase.Began ) 
            {
                _checkPlayerOut = false;
                player.isSelect = !player.isSelect;
            }
            else if (_hit.transform.CompareTag("Target")|_hit.transform.CompareTag("Material") && _touchScreen.phase == TouchPhase.Began)
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = false;
                _checkTwoFloor = false;
                _checkLadder = false;
            }
            else if (_hit.transform.CompareTag("Ladder") && _touchScreen.phase == TouchPhase.Began)
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = false;
            }
            else if (_hit.transform.CompareTag("TwoFloor") && _touchScreen.phase == TouchPhase.Began && player.isSelect )
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = false;
                _checkTwoFloor = true;
            }
            else if (_hit.transform.CompareTag("Action2") && _touchScreen.phase == TouchPhase.Began && player.isSelect )
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = true;
                _checkTwoFloor = true;
            }
            else if(_hit.transform.CompareTag("ActionHome") && _touchScreen.phase == TouchPhase.Began && player.isSelect)
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = true;
                _checkTwoFloor = false;
                _checkLadder = false;
            }
            else if(_hit.transform.CompareTag("Building") && _touchScreen.phase == TouchPhase.Began && player.isSelect)
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = true;
                _checkTwoFloor = false;
                _checkLadder = false;
            }
            else if(_hit.transform.CompareTag("Barricade") && _touchScreen.phase == TouchPhase.Began && player.isSelect)
            {
                UIManager.Instance.Name = "Non";
                _checkPlayerOut = true;
                player.isDoWork = true;
                _checkTwoFloor = false;
                _checkLadder = false;
            }
            else if(_hit.transform.name!=gameObject.name && player.isSelect | player.isDoWork &&_touchScreen.phase == TouchPhase.Began)
            {
                player.isSelect = false;
            }
        }
        void Move()
        {
            transform.position = Vector3.MoveTowards(transform.position,_target,speed*Time.deltaTime);
            player.animWalk.SetBool($"Walk{player.playerLv}",true);
            player.animWalk.SetBool($"Idle{player.playerLv}",false);
            player.animWalk.SetBool($"Attack{player.playerLv}",false);
            if (transform.position == _target)
            {
                player.animWalk.SetBool($"Walk{player.playerLv}",false);
                player.animWalk.SetBool($"Attack{player.playerLv}",false);
                player.animWalk.SetBool($"Idle{player.playerLv}",true);
                _checkMove = false;
                _checkPlayerOut = false;
                direcUI.DestroyDirecUI();
            }
        }
        private void SetPosition()
        {
            if (_checkPlayerOut)
            {
                 _target = _touch;
                 var position = transform.position;
                _setY = _target;
                _target.z = position.z;
                _target.y = position.y;
                _checkMove = true;
                player.isSelect = false;
                direcUI.OnInstant(_setY);
            }
            Rotation(_target);
        }
        public void Rotation(Vector3 target)
        {
            if (target.x<transform.position.x)
            {
                transform.eulerAngles = new Vector3 (0,180,0);
            }
            else if(target.x>transform.position.x)
            {
                transform.eulerAngles = new Vector3 (0,0,0);
            }
        }
        void Climbing()
        {
            if (_setY.y >= -0.5f)
            {
                transform.position += Vector3.up/climb;
            }
            if (transform.position.y >= -0.5f & _setY.y <= -0.5f)
            {
                transform.position += Vector3.down;
            }
        }
        private void OnTriggerStay2D(Collider2D other)
        {
            IBoolCheck check = other.GetComponent<IBoolCheck>();
            if (check!=null)
            {
                if (_setY.y >= -0.5f)
                {
                    _checkMove = false;
                    check.IsRotate0();
                    _checkClimbInOut = true;
                }
                if (_setY.y<= -0.5f && transform.position.y >= -0.5f)
                {
                    _checkMove = false;
                    check.IsRotate180();
                    _checkClimbInOut = true;
                }
            }
            if (other.CompareTag("SpawnPoint"))
            {
                Debug.Log("Moveee");
                _target = GameManager.Instance.playerBornPoint1.transform.position;
                Rotation(_target);
                Move();
            }
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Foot"))
            {
                _checkClimbInOut = false;
                _checkMove = true;
                _target.y = transform.position.y;
                Debug.Log("Is Grounded");
            }

            if (other.CompareTag("SpawnPoint"))
            {
                Debug.Log("Moveee");
                _target = GameManager.Instance.playerBornPoint1.transform.position;
                Rotation(_target);
                Move();
            }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Ladder"))
            {
                _checkClimbInOut = false;
                _checkMove = true;
            }
        }
        void FindLadderRight()
        {
            _bitMask = ~(1 << 0) & ~(1 << 8) & ~(1 << 5) & ~(1 << 9) & ~(1 << 10) & ~(1 << 12) & ~(1<<7)& ~(1<<15);
            var transform1 = transform;
            _hitLadder = Physics2D.Raycast(transform1.position, transform1.right, 100f,_bitMask);
            Debug.DrawRay(transform.position,transform.TransformDirection(100,0,0),Color.magenta);
            if (_hitLadder)
            {
                _ladder = _hitLadder.transform.position;
                var position = transform.position;
                _ladder.y = position.y;
                _ladder.z = position.z;
            }
            if (_setY.y<= -0.5f && transform.position.y >= -0.5f)
            {
                _checkLadder = true;
            }
        }
        void FindLadderLeft()
        {
            _bitMask = ~(1 << 0) & ~(1 << 8) & ~(1 << 5) & ~(1 << 9) & ~(1 << 10) & ~(1 << 12) & ~(1<<7)& ~(1<<15);
            var transform1 = transform;
            _hitLadder = Physics2D.Raycast(transform1.position, -transform1.right, 100f,_bitMask);
            Debug.DrawRay(transform.position,transform.TransformDirection(-100,0,0),Color.yellow);
            if (_hitLadder)
            {
                _ladder = _hitLadder.transform.position;
                var position = transform.position;
                _ladder.y = position.y;
                _ladder.z = position.z;
            }
        }
        void CheckToLadder()
        {
            if (_setY.y<= -0.5f & transform.position.y >= -0.5f & _checkTwoFloor)
            {
                _checkLadder = true;
                _checkMove = false;
            }

            if (_setY.y>= -0.5f & transform.position.y>= -0.5f & _checkTwoFloor )
            {
                Rotation(_target);
                _checkMove = true;
                _checkTwoFloor = false;
                _checkLadder = false;
            }

            if (_setY.y>= -0.5f & transform.position.y<= -0.5f & _checkTwoFloor)
            {
                _checkLadder = true;
                _checkMove = false;
            }
        }
        void MoveToLadder()
        {
            _checkMove = false;
            transform.position = Vector3.MoveTowards(transform.position,_ladder,speed*Time.deltaTime);
            player.animWalk.SetBool($"Walk{player.playerLv}",true);
            player.animWalk.SetBool($"Idle{player.playerLv}",false);
            player.animWalk.SetBool($"Attack{player.playerLv}",false);
            if (transform.position == _ladder)
            {
                player.animWalk.SetBool($"Walk{player.playerLv}",false);
                player.animWalk.SetBool($"Attack{player.playerLv}",false);
                player.animWalk.SetBool($"Idle{player.playerLv}",true);
                Debug.Log("Is Done");
                _checkLadder = false;
                _checkTwoFloor = false;
                Rotation(_target);
            }
            if (_ladder.x<transform.position.x)
            {
                transform.eulerAngles = new Vector3 (0,180,0);
            }
            else if(_ladder.x>transform.position.x)
            {
                transform.eulerAngles = new Vector3 (0,0,0);
            }
        }
        
    }

}
