﻿using Interface;

using UnityEngine;

namespace Scripe
{  
    
    [CreateAssetMenu(fileName = "Enemy",menuName = "EnemyClass")]
    public class EnemyFrom : ScriptableObject
    {
       public int   bite;
       public int   lifePoints;
       public int   attackDamage;
       public float moveSpeed ;
       public int   attackSpeed;
       public float   time;
      
    }
    
  
}
