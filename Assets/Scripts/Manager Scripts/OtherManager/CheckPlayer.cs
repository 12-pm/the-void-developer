using System;
using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class CheckPlayer : MonoBehaviour
    {
        private RaycastHit2D ray;

        private void Update()
        {
            //CheckPlayerInScene();
        }

        void CheckPlayerInScene()
        {
            var _bitMask = ~(1 << 0) & ~(1 << 14) & ~(1 << 5) & ~(1 << 9) & ~(1 << 10) & ~(1 << 12) & ~(1<<7)& ~(1<<15);
            var transform1 = transform;
            ray = Physics2D.Raycast(transform1.position, transform1.right, 100f,_bitMask);
            Debug.DrawRay(transform.position,transform.TransformDirection(100,0,0),Color.magenta);
            if (ray)
            {
                Debug.Log("see Player");
            }
            else
            {
                Debug.Log("No see Player");
                GameManager.Instance.playerList.Clear();
            }
        }
    }
}
