using System;
using Interface;
using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class ReinforcementManager : MonoBehaviour
    {
        private RaycastHit2D _hit;
        private bool _isClick;
        [SerializeField] private GameObject uiRf;

        private void Start()
        {
            _isClick = false;
            uiRf.SetActive(false);
        }

        private void Update()
        {
            RfPlayer();
        }

        void RfPlayer()
        {
            int bitMask = ~(1 << 9) & ~(1 << 10) & ~(1 << 12) & ~(1 << 5) & ~(1 << 0) & ~(1 << 7);
            var position = transform.position;
            _hit = Physics2D.CircleCast(position, 1.0f, position, 1.0f, bitMask);
            if (_hit)
            {
                uiRf.SetActive(true);
                var gm = GameManager.Instance;
                ISPlayerDoWork player = _hit.transform.GetComponent<ISPlayerDoWork>();
                if (player!=null & _isClick)
                {
                    gm.SpawnPlayer(gm.playerBornPoint2.transform.position);
                    _isClick = false;
                }
            }
            else
            {
                uiRf.SetActive(false);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                uiRf.SetActive(false);
            }
        }

        public void ClickRf(int rq)
        {
            var inventory = InventoryManager.Instance.inventory;
            if (inventory.ContainsKey("Food"))
            {
                if (inventory["Food"]>=rq)
                {
                    _isClick = true;
                    inventory["Food"] -= rq;
                }
                else
                {
                    Debug.Log("You don't have any material more");
                }
            }
            else
            {
                Debug.Log("You don't have material");
            }
            
        }
    }
}
