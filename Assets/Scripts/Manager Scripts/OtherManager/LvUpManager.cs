using System;
using Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Manager_Scripts.OtherManager
{
    public class LvUpManager : MonoBehaviour
    {
        private RaycastHit2D _hit;
        private int _lv;
        private string material1;
        private int rqM1;
        private string material2;
        private int rqM2;
        private bool _isSelect;
        private bool _isSeePlayer;
        //UI
        [SerializeField] private GameObject uiSelect;

        private void Start()
        {
            UiSetActive(false);
            _isSelect = false;
        }

        private void Update()
        {
            CheckAndSetPlayerLv();
        }

        void CheckAndSetPlayerLv()
        {
            int bitMask = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _hit = Physics2D.CircleCast(position, 1.0f, position, 1.0f, bitMask);
            if (_hit)
            {
                IPlayerLv playerLv = _hit.transform.GetComponent<IPlayerLv>();
                ISPlayerDoWork playerDoWork = _hit.transform.GetComponent<ISPlayerDoWork>();
                if (playerDoWork != null)
                {
                    if (playerLv!=null & playerDoWork.IsDoingWork())
                    {
                        UiSetActive(true);
                        if (playerLv.PlayerLv() != _lv & _isSelect)
                        {
                            playerLv.SetPlayerLv(_lv);
                            _isSelect = false;
                            UiSetActive(false);
                            playerDoWork.IsDoingWorkSet(false);
                        }
                        else
                        {
                            Debug.Log("You are now level : "+_lv);
                        }
                    }
                    else
                    {
                        UiSetActive(false);
                    }
                }
            }
            else
            {
                UiSetActive(false);
            }
        }
        public void SelectAndCondition(int lv)
        {
            var inventory = InventoryManager.Instance.inventory;
            
            for (int i = 1; i < 5; i++)
            {
                if (lv==i & inventory.ContainsKey(material1) & inventory.ContainsKey(material2))
                {
                    if (inventory[material1]>=rqM1 & inventory[material2]>=rqM2)
                    {
                        _lv = lv;
                        _isSelect = true;
                        inventory[material1] -= rqM1; inventory[material2] -= rqM2;
                    }
                    else
                    {
                        Debug.Log("You don't have any material more");
                    }
                }
                else
                {
                    Debug.Log("You don't have material");
                }
            }
        }

        void UiSetActive(bool set)
        {
            uiSelect.SetActive(set);
        }

        public void Setmaterial1(string name)
        {
            material1 = name;
        }
        public void Setmaterial2(string name)
        {
            material2 = name;
        }
        public void SetRqM1(int quantity)
        {
            rqM1 = quantity;
        }
        public void SetRqM2(int quantity)
        {
            rqM2 = quantity;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                UiSetActive(false);
            }
        }
    }
}
