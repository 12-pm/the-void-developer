﻿using System;
using FileSO.SpawnSO;
using Manager_Scripts.Enemy.Scripe;
using Manager_Scripts.OtherManager;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Serialization;

namespace Manager_Scripts
{
    public class SpawnManager : MonoSingleton<SpawnManager>
    {
        public Transform[] spawnPoints;
        public Rigidbody2D[] enemyPrefabs;
        private float Timer;
        private bool isDo1;
        private bool isDo2;
        private bool isDo3;
        private bool isDo4;
        public float SpawnTimer;
        public float Time1;
        public float Time2;
        public float Time3;
        //Set1
        public int Enemies11;
        public int Enemies12;
        public int Enemies13;
        public int Enemies14;
        
        public int EnemyForm11;
        public int EnemyForm12;
        public int EnemyForm13;
        public int EnemyForm14;
        //Set2
        public int Enemies21;
        public int Enemies22;
        public int Enemies23;
        public int Enemies24;
        
        public int EnemyForm21;
        public int EnemyForm22;
        public int EnemyForm23;
        public int EnemyForm24;
        //Set3
        public int Enemies31;
        public int Enemies32;
        public int Enemies33;
        public int Enemies34;
        
        public int EnemyForm31;
        public int EnemyForm32;
        public int EnemyForm33;
        public int EnemyForm34;
        
        
        private int EnemyCount;

        private int[] setC1;
        private int[] setF1;
        private int[] setC2;
        private int[] setF2;
        private int[] setC3;
        private int[] setF3;
        private int[] setC4;
        private int[] setF4;

        //[SerializeField] private EnemyMainGame enemyMainGame;
        private int CurrentDay;
        public int MainEnemyForm;
        
        int randSpawnPoint;
        private bool daychange;
        private bool EndSpawn;
        private bool EndSpawn2;
        private bool EndSpawn3;
        private int endCount;
        private bool isGenerate;
       
       [SerializeField] private SpawnSo[] So;
       void SetSo(int day)
        {
            Time1 = So[day].Time1;
            Time2 = So[day].Time2;
            Time3 = So[day].Time3;
            //Set1
            Enemies11 = So[day].Enemies11;
            Enemies12 = So[day].Enemies12;
            Enemies13 = So[day].Enemies13;
            Enemies14 = So[day].Enemies14;
            EnemyForm11 = So[day].EnemyForm11;
            EnemyForm12 = So[day].EnemyForm12;
            EnemyForm13 = So[day].EnemyForm13;
            EnemyForm14 = So[day].EnemyForm14;
            //Set2
            Enemies21 = So[day].Enemies21;
            Enemies22 = So[day].Enemies22;
            Enemies23 = So[day].Enemies23;
            Enemies24 = So[day].Enemies24;
            EnemyForm21 = So[day].EnemyForm21;
            EnemyForm22 = So[day].EnemyForm22;
            EnemyForm23 = So[day].EnemyForm23;
            EnemyForm24 = So[day].EnemyForm24;
            //Set3
            Enemies31 = So[day].Enemies31;
            Enemies32 = So[day].Enemies32;
            Enemies33 = So[day].Enemies33;
            Enemies34 = So[day].Enemies34;
            EnemyForm31 = So[day].EnemyForm31;
            EnemyForm32 = So[day].EnemyForm32;
            EnemyForm33 = So[day].EnemyForm33;
            EnemyForm34 = So[day].EnemyForm34;

        }

       void SetCAndF()
       {
           setC1 = new[] {Enemies11, Enemies12, Enemies13, Enemies14};
           setF1 = new[] {EnemyForm11, EnemyForm12, EnemyForm13, EnemyForm14};
           setC2 = new[] {Enemies21, Enemies22, Enemies23, Enemies24};
           setF2 = new[] {EnemyForm21, EnemyForm22, EnemyForm23, EnemyForm24};
           setC3 = new[] {Enemies31, Enemies32, Enemies33, Enemies34};
           setF3 = new[] {EnemyForm31, EnemyForm32, EnemyForm33, EnemyForm34};
       }
        
        void Start()
        {
            daychange = true;
            CurrentDay = UIManager.Instance._dayCount;
            EnemyCount = 0;
            Enemies11 = 0;
            EndSpawn = false;
            EndSpawn2 = true;
            EndSpawn3 = true;
            endCount = 1;
            isGenerate = false;
            SetDo(true);
        }

        void SetDo(bool set)
        {
            isDo1 = set;
            isDo2 = set;
            isDo3 = set;
            isDo4 = set;
        }
        void Spawn()
        {
            if (CurrentDay == UIManager.Instance._dayCount  & UIManager.Instance.time >= Time1)
            {
                if (EndSpawn == false)
                {
                    //Debug.Log("C1");
                    SetSpawn(setC1,setF1);
                    if (isGenerate == false)
                    {
                        EndSpawn = true;
                        EndSpawn2 = false;
                        EndSpawn3 = true;
                        endCount = 1;
                        EnemyCount = 0;
                        SetDo(true);
                    }
                }
            } 
            if (CurrentDay == UIManager.Instance._dayCount & UIManager.Instance.time >= Time2)
            {
                if (EndSpawn2 == false )
                {
                    //Debug.Log("C2");
                    SetSpawn(setC2,setF2);
                    if (isGenerate == false)
                    {
                        EndSpawn = true;
                        EndSpawn2 = true;
                        EndSpawn3 = false;
                        endCount = 1;
                        SetDo(true);
                    }
                }
                
            }
            if (CurrentDay == UIManager.Instance._dayCount & UIManager.Instance.time >= Time3)
            {
                if (EndSpawn3 == false )
                {
                    //Debug.Log("C3");
                    SetSpawn(setC3,setF3);
                    if(isGenerate == false)
                    {
                        EndSpawn = true;
                        EndSpawn2 = true;
                        EndSpawn3 = true;
                        endCount = 1;
                        SetDo(true);
                    }
                }
                
            }
        }

        void SetSpawn(int[]enemy,int[]from)
        {
            //Debug.Log("is EnemyCount "+EnemyCount);
            if (isDo1)
            {
                if (EnemyCount<enemy[0] && endCount == 1 && enemy[0] != 0 && Timer>=SpawnTimer)
                {
                    isGenerate = true;
                    Debug.Log("S1");
                    Spawner(from[0]);
                    Timer = 0;
                    EnemyCount+=1;
                }
                if(EnemyCount>=enemy[0] && endCount == 1 && enemy[0] != 0)
                {
                    isDo1 = false;
                    EnemyCount = 0;
                    isGenerate = false;
                    endCount = 2;
                    //Debug.Log("is endCount"+endCount);
                }
            }

            if (isDo2)
            {
                if (EnemyCount<enemy[1] & endCount == 2 & enemy[1] != 0)
                {
                    isGenerate = true;
                    if (Timer>=SpawnTimer)
                    {
                        //Debug.Log("S2");
                        Spawner(from[1]);
                        Timer = 0;
                        EnemyCount += 1;
                    }
                }
                if(EnemyCount>=enemy[1] & endCount == 2 & enemy[1] != 0)
                {
                    isDo2 = false;
                    isGenerate = false;
                    EnemyCount = 0;
                    endCount = 3;
                    //Debug.Log("is endCount"+endCount);
                }
            }

            if (isDo3)
            {
                if (EnemyCount<enemy[2] & endCount == 3 & enemy[2] != 0)
                {
                    isGenerate = true;
                    if (Timer>=SpawnTimer)
                    {
                        //Debug.Log("S3");
                        Spawner(from[2]);
                        Timer = 0;
                        EnemyCount += 1;
                    }
                }
                if(EnemyCount>=enemy[2] & endCount == 3 & enemy[2] != 0)
                {
                    isDo3 = false;
                    isGenerate = false;
                    EnemyCount = 0;
                    endCount = 4;
                    //Debug.Log("is endCount"+endCount);
                }
            }

            if (isDo4)
            {
                if (EnemyCount<enemy[3] & endCount == 4 & enemy[3] != 0)
                {
                    isGenerate = true;
                    if (Timer>=SpawnTimer)
                    {
                        //Debug.Log("S4");
                        Spawner(from[3]);
                        Timer = 0;
                        EnemyCount += 1;
                    }
                }
                if(EnemyCount>=enemy[3] & endCount == 4 & enemy[3] != 0)
                {
                    isDo4 = false;
                    isGenerate = false;
                    EnemyCount = 0;
                    endCount = 5;
                    //Debug.Log("is endCount"+endCount);
                }
            }
            
        }
        void Spawner(int form)
        {
            MainEnemyForm = form;
            Rigidbody2D clone = Instantiate(enemyPrefabs[0], spawnPoints[randSpawnPoint].transform.position,Quaternion.identity);
        }
        void Update()
        {
            //Debug.Log("Is the Enmy : "+EnemyCount);
            randSpawnPoint = Random.Range(0, spawnPoints.Length);
            Timer += Time.deltaTime;
            if ( UIManager.Instance._dayCount <= CurrentDay)
            {
                daychange = true;
            }
            else
            {
                endCount = 1;
                daychange = false;
                CurrentDay = UIManager.Instance._dayCount;
                EndSpawn = false;
            }
            if (daychange)
            {
                SetSo(UIManager.Instance._dayCount);
                SetCAndF();
                Spawn();
            }
            if (GameManager.Instance.gameEnd)
            {
                Start();
            }
        }
        
        
        
        
    }
}