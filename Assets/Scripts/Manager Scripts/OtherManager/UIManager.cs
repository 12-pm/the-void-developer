﻿using System;
using Manager_Scripts.OtherManager;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager_Scripts
{
    public class UIManager : MonoSingleton<UIManager>
    {
        [SerializeField] private GameObject UIPlay;
        [SerializeField] private GameObject UIScore;
        [SerializeField] private GameObject UiLogin;
        [SerializeField] private GameObject UiWin;
        [SerializeField] private GameObject UiLose;
        [SerializeField] private GameObject Clock;
        [SerializeField]internal float time;
        [SerializeField] private float timeSpeed;
        //Text
        [SerializeField] private Text Day;
        [SerializeField] private Text foodText;
        [SerializeField] private Text woodText;
        [SerializeField] private Text electText;
        [SerializeField] private Text scrapText;
        [SerializeField] private Text player;

        [SerializeField] private Text score;
        //
        [SerializeField] private GameObject uiOther;

        [SerializeField] private FireBaseConTroller fireBase;
        public int _dayCount;
        public Text selectPlayerName;
        internal string Name = "Non";
        private bool _setActive;
        private bool _setActiveUiScore;
        public bool isClickQuit;
        public bool isClickPlay;
        private bool isClickOther;
        public int showday;
        public bool isTutorial;

        int    scoree;
        private string namee;
        
        public GameObject uiElement, pauseUi;

        public static bool GamePauses = false;
        
        

        private void Start()
        {
            if (isTutorial==false)
            {
                isClickQuit = false;
                isClickOther = false;
                Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
                Time.timeScale = 0f;
                time = 540;
                UiLogin.SetActive(true);
                showday = 1;
                _dayCount = 0;
                UiWin.SetActive(false);
                UiLose.SetActive(false);
                UIPlay.SetActive(false);
                UIScore.SetActive(false);
                Debug.Log(_dayCount); 
                uiElement.SetActive(false);
                pauseUi.SetActive(false);
                uiOther.SetActive(false);
                CheckAndShowText();
            }
        }
        
        private void Update()
        {
            CheckAndShowText();
            if (GameManager.Instance.gameEnd)
            {
                Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
                Time.timeScale = 0f;
                time = 540;
                showday = 1;
                _dayCount = 0;
            }
            time += Time.deltaTime*timeSpeed;
            if (time>=1260)
            {
                time = 540;
                showday += 1;
                _dayCount += 1;
                Debug.Log(_dayCount);
            }
            Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.back*time);
            selectPlayerName.text = $"Select Hero: {Name}";
            Day.text = $"Day : {showday}";
            if (_setActive)
            {
                UIPlay.SetActive(true);
            }
            else
            {
                UIPlay.SetActive(false);
            }
            if (_setActiveUiScore)
            {
                UIScore.SetActive(true);
            }
            else
            {
                UIScore.SetActive(false);
            }
            
        }

        public void ClickOther()
        {
            isClickOther = !isClickOther;
            uiOther.SetActive(isClickOther);
        }

        public void OnClick()
        {
            _setActive = !_setActive;
        }

        public void OnClickUIPlay()
        {
            _setActiveUiScore = !_setActiveUiScore;
        }

        public void Pause()
        {
            pauseUi.SetActive(true);
            Time.timeScale = 0f;
            GamePauses = true;
        }
        public  void Continue()
        {
            pauseUi.SetActive(false);
            Time.timeScale = 1f;
            GamePauses = false;
        }

        public void ClickPlay()
        {
            InventoryManager.Instance.ChekAndAdd("Food",1000);
            InventoryManager.Instance.ChekAndAdd("Wood",1000);
            InventoryManager.Instance.ChekAndAdd("Scrap",1000);
            InventoryManager.Instance.ChekAndAdd("Electronic",1000);
            GameManager.Instance.SpawnBase();
            GameManager.Instance.SpawnPlayerStart();
            GameManager.Instance.gameStart = false;
            isClickPlay = true;
            Time.timeScale = 1f;
            UiLogin.SetActive(false);
            UiLose.SetActive(false);
            UiWin.SetActive(false);
            uiElement.SetActive(true);
            GameManager.Instance.gameEnd = false; 
            LB_Controller.instance.StoreScore(GameManager.Instance.ShowScore(), $"{fireBase.profileUser_Name}");
            LB_Controller.instance.ReloadLeaderboard();
        }

        public void ClickQuit()
        {
            
            isClickQuit = true;
            isClickPlay = false;
            Debug.Log("Click Quit");
            UiLogin.SetActive(true);
            uiElement.SetActive(false);
            UiWin.SetActive(false);
            UiLose.SetActive(false);
            pauseUi.SetActive(false);
            GameManager.Instance.gameStart = true;
            GameManager.Instance.gameEnd = true;
            Time.timeScale = 1f;
            scoree = GameManager.Instance.ShowScore();
            namee = fireBase.profileUser_Name.text;
            LB_Controller.instance.StoreScore(scoree, $"{namee}");
        }

        public void GameWin()
        {
            Time.timeScale = 0f;
            UiWin.SetActive(true);
        }

        public void GameLose()
        {
            Time.timeScale = 0f;
            UiLose.SetActive(true);
            isClickQuit = true;
        }

        public void GameQuit()
        {
            Application.Quit();
        }

        void CheckAndShowText()
        {
            var inventory = InventoryManager.Instance.inventory;
            foodText.text = inventory.ContainsKey("Food") ? $"{inventory["Food"]}" : "0";
            woodText.text = inventory.ContainsKey("Wood") ? $"{inventory["Wood"]}" : "0";
            electText.text = inventory.ContainsKey("Electronic") ? $"{inventory["Electronic"]}" : "0";
            scrapText.text = inventory.ContainsKey("Scrap") ? $"{inventory["Scrap"]}" : "0";
            player.text = $"{GameManager.Instance.playerList.Count}";
            score.text = $"Score : {GameManager.Instance.ShowScore()}";
        }
    }
}