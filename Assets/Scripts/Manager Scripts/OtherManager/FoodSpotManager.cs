﻿using System;
using Interface;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager_Scripts.OtherManager
{
    public class FoodSpotManager : MonoBehaviour
    {
        private RaycastHit2D[] _hit;
        [SerializeField]private float plusHungry;
        [SerializeField] private GameObject uiHungry;
        private int eater;
        private bool isPay;
        private bool clickSelect;
        private bool clickPay;
        [SerializeField] private Text textSelect;
        [SerializeField] private Text textMaterial;
        [SerializeField] private GameObject uiPay;
        private float timeUi;
        private void Start()
        {
            timeUi = 0.0f;
            eater = 0;
            isPay = false;
            clickSelect = false;
            uiHungry.SetActive(false);
            uiPay.SetActive(false);
        }
        private void Update()
        {
            textSelect.text = "x"+eater;
            textMaterial.text = "-" + eater;
            if (clickPay)
            {
                timeUi += Time.deltaTime;
            }

            if (timeUi>=2.0f)
            {
                uiPay.SetActive(false);
                timeUi = 0;
            }
            CheckPlayer();
        }

        void CheckPlayer()
        {
            int bitMask = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _hit = Physics2D.CircleCastAll(position, 0.5f, position, 0.5f, bitMask);
            foreach (var hit in _hit)
            {
                IHungry hungry = hit.transform.GetComponent<IHungry>();
                ISPlayerDoWork player = hit.transform.GetComponent<ISPlayerDoWork>();
                if (hungry!=null & player!=null)
                {
                    if (player.IsDoingWork())
                    {
                        uiHungry.SetActive(true);
                    }
                    

                    if (player.IsGenerate()==false & clickSelect & eater < 3)
                    {
                        eater += 1;
                        player.IsGenerateSet(true);
                        clickSelect = false;
                    }

                    if (player.IsGenerate() & isPay)
                    {
                        hungry.HungryPlus(plusHungry);
                        isPay = false;
                    }
                }
            }
        }
        public void ClickFeed()
        {
            var inventory = InventoryManager.Instance.inventory;
            if (isPay==false)
            {
                if (inventory.ContainsKey("Food"))
                {
                    if (inventory["Food"]>=1*eater)
                    {
                        inventory["Food"] -= 1*eater;
                        isPay = true;
                        clickPay = true;
                        uiPay.SetActive(true);
                    }
                }
            }
        }
        public void ClickSelect()
        {
            clickSelect = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            ISPlayerDoWork doWork = other.transform.GetComponent<ISPlayerDoWork>();
            if (doWork!=null)
            {
                if (doWork.IsGenerate())
                {
                    doWork.IsGenerateSet(false);
                    eater -= 1;
                }

                if (eater<=0)
                {
                    uiHungry.SetActive(false);
                }
            }
        }
    }
}