using UnityEngine;
using UnityEngine.UI;

namespace Manager_Scripts.OtherManager
{
    public class MoveCamera : MonoBehaviour
    {
        [SerializeField] private Transform camera;
        [SerializeField] private Slider slider;
        [SerializeField] private GameObject button;
        void Start()
        {
            slider.value = 0.0f;
            slider.maxValue = 30.0f;
            slider.minValue = -30.0f;
            camera.position = new Vector2(slider.value,camera.position.y);
        }

   
        void Update()
        {
            if (GameManager.Instance.gameEnd)
            {
                Start();
            }
            camera.position = new Vector2(slider.value,camera.position.y);
            if (slider.value!=0.0f)
            {
                button.SetActive(true);
            }
            else
            {
                button.SetActive(false);
            }
        }

        public void ResetCamera()
        {
            slider.value = 0;
        }
    }
}
