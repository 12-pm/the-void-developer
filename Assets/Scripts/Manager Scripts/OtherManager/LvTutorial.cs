using System;
using Interface;
using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class LvTutorial : MonoBehaviour
    {
        private RaycastHit2D _hit;

        private void Update()
        {
            CheckPlayer();
        }

        void CheckPlayer()
        {
            int bitMask = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _hit = Physics2D.CircleCast(position, 100.0f, position, 1.0f, bitMask);
            if (_hit)
            {
                IHungry player = _hit.transform.GetComponent<IHungry>();
                if (player!=null)
                {
                    player.HpPlus(999999);
                    player.HungryPlus(9999999);
                }
            }
        }
    }
}
