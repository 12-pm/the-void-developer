using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using System;
using System.Threading.Tasks;
using Firebase.Extensions;


public class FireBaseConTroller : MonoBehaviour
{
    public GameObject loginPanel, singupPanel, profilePanel, forgetPasswordPanel, ErrorPanal;//playPanal;
    public InputField loginEmail ,loginPassWord,signupEmail, signupPassword, signConPassword, signupUserName,forgetPassEmail;
    public Text error_Text, errorMassage_Text,profileUser_Name,profileUser_Email;
    public Toggle rememberMe;

    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;
    private bool isSingin = false;
    public void Start()
    {
       
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available) {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                InitializeFirebase();

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            } else {
                UnityEngine.Debug.LogError(System.String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    public void OpLoginPa()
    {
        loginPanel.SetActive(true);
        singupPanel.SetActive(false);
        profilePanel.SetActive(false);
        forgetPasswordPanel.SetActive(false);
    }

    public void OpSingupPa()
    {
       // playPanal.SetActive(false);
        loginPanel.SetActive(false);
        singupPanel.SetActive(true);
        profilePanel.SetActive(false);
        forgetPasswordPanel.SetActive(false);
    }

    public void OpProPa()
    {
       // playPanal.SetActive(false);
        loginPanel.SetActive(false);
        singupPanel.SetActive(false);
        profilePanel.SetActive(true);
        forgetPasswordPanel.SetActive(false);
    }

    public void OpForgetPassPa()
    {
        {
           // playPanal.SetActive(false);
            loginPanel.SetActive(false);
            singupPanel.SetActive(false);
            profilePanel.SetActive(false);
            forgetPasswordPanel.SetActive(true);
        }
    }
    public void OpPlayPa()
    {
        { 
           
            loginPanel.SetActive(false);
            singupPanel.SetActive(false);
            profilePanel.SetActive(false);
            forgetPasswordPanel.SetActive(false);
            //playPanal.SetActive(true);
        }
    }

    public void LoginUser()
    {
        if (string.IsNullOrEmpty(loginEmail.text) && string.IsNullOrEmpty(loginPassWord.text))
        {
            ErrorPA("Error","Some fields empty please fill all fields");
            return;
        }
        /*/
        if (string.IsNullOrEmpty(loginEmail.text) )
        {
            ErrorPA("Error","Some fields empty please fill all fields");
            return;
        }
        if ( string.IsNullOrEmpty(loginPassWord.text))
        {
            ErrorPA("Error","Some fields empty please fill all fields");
            return;
        }
        */
        //login
        signInUser(loginEmail.text,loginPassWord.text);
      
        
    }

    public void SignUpUser()
    {
        if (string.IsNullOrEmpty(signupEmail.text)&&string.IsNullOrEmpty(signupPassword.text)&&string.IsNullOrEmpty(signupUserName.text)&&string.IsNullOrEmpty(signConPassword.text))
        {
            ErrorPA("Error"," please fill all fields");
          return;  
        }
        /*
        if (string.IsNullOrEmpty(signupEmail.text))
        {
            ErrorPA("Error","Email empty please fill all fields");
            return;  
        }
        if (string.IsNullOrEmpty(signupPassword.text))
        {
            ErrorPA("Error","Password empty please fill all fields");
            return;  
        }
        if (string.IsNullOrEmpty(signupUserName.text))
        {
            ErrorPA("Error","UserName empty please fill all fields");
            return;  
        }

        if (string.IsNullOrEmpty(signConPassword.text))
        {
            ErrorPA("Error","ConfirmePassword empty please fill all fields");
            return;  
        }*/

        //SignUp
        CreateUser(signupEmail.text,signupPassword.text,signupUserName.text);
    }

    public void ForgetPass()
    {
        if (string.IsNullOrEmpty(forgetPassEmail.text))
        {
            ErrorPA("Error"," Field Email Empty");
            return;  
        }
    }

    private void ErrorPA(string title, string message)
    {
        error_Text.text = "" + title;
        errorMassage_Text.text = "" + message;
        ErrorPanal.SetActive(true);
    }

    public void CloseErrorPA()
    {
        
        error_Text.text = "" ;
        errorMassage_Text.text = "" ;
        ErrorPanal.SetActive(false);
    }

    public void LogOut()
    {
        auth.SignOut();
        profilePanel.SetActive(false);
        profileUser_Email.text = "";
        profileUser_Name.text = "";
        OpLoginPa();
    }

    public void CreateUser(string email, string password,string Username)
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task => {
            if (task.IsCanceled) {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
                {
                    Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                    if (firebaseEx != null)
                    {
                        var errorCode = (AuthError) firebaseEx.ErrorCode;
                        ErrorPA("Error", GetErrorMessage(errorCode));
                    }
                }

                return;
                
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})", 
                newUser.DisplayName, newUser.UserId);
             UpdateUserProflie(Username);
             ErrorPA("Account Created"," Account Successfully Created");
             OpProPa();
        });
        
    }

    public void signInUser(string email ,string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task => {
            if (task.IsCanceled) {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
                {
                    Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                    if (firebaseEx != null)
                    {
                        var errorCode = (AuthError) firebaseEx.ErrorCode;
                        ErrorPA("Error", GetErrorMessage(errorCode));
                    }
                }

                return;
            }
 
           
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            profileUser_Name.text = "" + newUser.DisplayName;
            profileUser_Email.text = "" + newUser.Email;
            OpProPa();
        });
    }
    void InitializeFirebase() {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs) {
        if (auth.CurrentUser != user) {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null) {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn) {
                Debug.Log("Signed in " + user.UserId);
                isSingin = true;
            }
        }
    }
   void OnDestroy() {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

    void UpdateUserProflie(string UserName)
    {
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null) {
            Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile {
                DisplayName = UserName,
                PhotoUrl = new System.Uri("https://example.com/jane-q-user/profile.jpg"),
            };
            user.UpdateUserProfileAsync(profile).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                    return;
                }
                Debug.Log("User profile updated successfully.");
              
            });
        }
    }

    private bool isSigned = false;
    private void Update()
    {
        if (isSingin)
        {
            if (!isSigned)
            {
                isSigned = true;
                profileUser_Name.text = "" + user.DisplayName;
                profileUser_Email.text = "" + user.Email;
                OpProPa();
            }
        }
    }
    private static string GetErrorMessage(AuthError errorCode)
    {
        var message = "";
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                message = "Account not Exist";
                break;
            case AuthError.MissingPassword:
                message = "MissingPassword";
                break;
            case AuthError.WeakPassword:
                message = "WeakPassword";
                break;
            case AuthError.WrongPassword:
                message = "WrongPassword";
                break;
            case AuthError.EmailAlreadyInUse:
                message = "Email used by other account";
                break;
            case AuthError.InvalidEmail:
                message = "invalid Email";
                break;
            case AuthError.MissingEmail:
                message = "MissingEmail";
                break;
            default:
                message = "invalid error";
                break;
        }
        return message;
    }
}
