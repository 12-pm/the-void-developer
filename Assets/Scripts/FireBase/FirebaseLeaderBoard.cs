using System;
using System.Collections;
using System.Collections.Generic;
using Firebase;
using Manager_Scripts.OtherManager;
using Models;
using UnityEngine;
using Proyecto26;
using SimpleJSON;


public class FirebaseLeaderBoard : MonoBehaviour
{
    public string urlFireBase = "https://login-thevoid-default-rtdb.asia-southeast1.firebasedatabase.app/";
    public string toKenFireBase = "OJ6HA9OyIeV63UJcARA0oJtXn2PAdqWhuATTv9U3";
    [System.Serializable]
    public class User
    {
        [System.Serializable]
        public class UserDataaa
        {
            public string name;
            public int rank;
            public int score;
            public UserDataaa(string userNameInLederBoardd, int rankInLederBoardd, int scooreInLederBoardd)
            {
                this.name = userNameInLederBoardd;
                this.rank = rankInLederBoardd;
                this.score = scooreInLederBoardd;
            }
        }
        public List<UserDataaa> UserData;
    }
    public User user;
    // Start is called before the first frame update
    void Start()
    {
        Getdata();
    }
    public void Getdata()
    {
        string urlData = $"{urlFireBase}/User/UserData.json?auth={toKenFireBase}";
        user.UserData = new List<User.UserDataaa>();
        RestClient.Get(urlData).Then(response =>
        {
            Debug.Log(urlData);
            Debug.Log(response.Text);
            JSONNode jsonNode = JSONNode.Parse(response.Text);
            for (int i = 0; i < jsonNode.Count; i++)
            {
                user.UserData.Add(new User.UserDataaa(jsonNode[i]["name"],jsonNode[i]["rank"],jsonNode[i]["score"]));
                GameManager.Instance.SetScore(jsonNode[i]["score"]);
            }
            
        }).Catch(error =>
        {
            Debug.Log("error");
        });
    }

    public void SetData()
    {
        string urlData = $"{urlFireBase}/User.json?auth={toKenFireBase}";
        RestClient.Get(urlData).Then(response =>
        {
            Debug.Log(urlData);
            Debug.Log(response.Text);
            RestClient.Put<User>(urlData,user).Then(response =>
            {
                Debug.Log("Uploading........");
                Debug.Log("complete");
            }).Catch(error =>
            {
               Debug.Log("error on set data to server"); 
            });
         
        }).Catch(error =>
        {

            Debug.Log("error");
        });
    }
}
