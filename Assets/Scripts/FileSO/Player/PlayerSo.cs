﻿using UnityEngine;
using UnityEngine.Serialization;

namespace FileSO.Player
{
    [CreateAssetMenu(fileName = "Player", menuName = "Player")]
    public class PlayerSo : ScriptableObject
    {
        public float fireRate;
        public int maxHp;
        public new string name;
        public int damage;
        public float vision;
        public float speed;
        public float maxHungry;
        public float hungryReduce;
        public AudioClip Sound;
    }
}