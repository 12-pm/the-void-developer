using UnityEngine;

namespace FileSO.SpawnSO
{
    [CreateAssetMenu(fileName = "Day1", menuName = "SpawnSo")]
    public class SpawnSo : ScriptableObject
    {
        //Set1
        public string set1;
        public float Time1;
        public int Enemies11;
        public int EnemyForm11;
        public int Enemies12;
        public int EnemyForm12;
        public int Enemies13;
        public int EnemyForm13;
        public int Enemies14;
        public int EnemyForm14;
        
        //Set2
        public string set2;
        public float Time2;
        public int Enemies21;
        public int EnemyForm21;
        public int Enemies22;
        public int EnemyForm22;
        public int Enemies23;
        public int EnemyForm23;
        public int Enemies24;
        public int EnemyForm24;
        
        //Set3
        public string set3;
        public float Time3;
        public int Enemies31;
        public int EnemyForm31;
        public int Enemies32;
        public int EnemyForm32;
        public int Enemies33;
        public int EnemyForm33;
        public int Enemies34;
        public int EnemyForm34;
    }
}