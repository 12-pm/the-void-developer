using UnityEngine;

namespace FileSO.Factory
{
    [CreateAssetMenu(fileName = "Factory", menuName = "Factory")]
    public class FactorySo : ScriptableObject
    {
        public int quantity;
        public new string name;
        public float time;
        public string mtName1;
        public int mt1;
        public string mtName2;
        public int mt2;
        public AudioClip sound;
    }
}
