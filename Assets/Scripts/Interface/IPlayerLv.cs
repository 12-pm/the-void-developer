﻿namespace Interface
{
    public interface IPlayerLv
    {
        public void SetPlayerLv(int lv);
        public int PlayerLv();
    }
}